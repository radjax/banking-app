﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1 {
    public class Account {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public decimal AccountBalance { get; set; }
        public List<Transaction> TransactionHistory { get; set; }

    }
}
