﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public static class LoginRepository
    {
        static List<User> _userList = new List<User>();
        static List<Account> _accountList = new List<Account>();

        public static bool CreateUser(string username, string password, string firstName, string lastName) {
            Guid accountId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName)) {
                return false;
            }
            string passwordHashed = Hash(password);

            User user = new User {
                UserId = userId,
                Username = username,
                Password = passwordHashed,
                FirstName = firstName,
                LastName = lastName
            };

            Account account = new Account {
                AccountId = accountId,
                UserId = userId,
                AccountBalance = 0.00m,
                TransactionHistory = new List<Transaction>()
            };

            _userList.Add(user);
            _accountList.Add(account);
            return true;
        }

        public static User Login(string username, string password) {
           
            User user = _userList.Where(x => x.Username == username).FirstOrDefault();
            if (user != null && password != null) {
                bool validPass = CheckHash(password, user.Password);
                return validPass ? user : null;
            } else {
                return null;
            }
        }

        public static decimal DepositFunds(Guid accountId, decimal amount) {
            Account account = _accountList.Where(x => x.AccountId == accountId).FirstOrDefault();
            Transaction transaction = new Transaction {
                TransactionId = Guid.NewGuid(),
                AccountId = accountId,
                TransactionType = "Deposit",
                TransactionAmount = amount.ToString("C2"),
                TransactionDate = DateTime.Now
            };
            account.AccountBalance += amount;
            account.TransactionHistory.Add(transaction);
            return account.AccountBalance;
        }

        public static decimal WithdrawFunds(Guid accountId, decimal amount) {
            amount = amount * -1.00m;
            Account account = _accountList.Where(x => x.AccountId == accountId).FirstOrDefault();
            Transaction transaction = new Transaction {
                TransactionId = Guid.NewGuid(),
                AccountId = accountId,
                TransactionType = "Withdrawal",
                TransactionAmount = amount.ToString("C2"),
                TransactionDate = DateTime.Now
            };
            account.AccountBalance += amount;
            account.TransactionHistory.Add(transaction);
            return account.AccountBalance;
        }

        public static List<Transaction> GetTransactions(Guid accountId) {
            return _accountList.Where(x => x.AccountId == accountId).FirstOrDefault().TransactionHistory;
        }

        public static decimal GetBalance(Guid accountId) {
            return _accountList.Where(x => x.AccountId == accountId).FirstOrDefault().AccountBalance;
        }

        public static User GetUser(Guid userId) {
            return _userList.Where(x => x.UserId == userId).FirstOrDefault();
        }

        public static Account GetAccount(Guid userId) {
            return _accountList.Where(x => x.UserId == userId).FirstOrDefault();
        }

        public static string Hash(string password) {

            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 16480);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }
        public static bool CheckHash(string password, string savedPasswordHash) { 
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 16480);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i]) {
                    return false;
                }

            return true;
        }
    }
}
