﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1 {
    public class Transaction {
        public Guid TransactionId { get; set; }
        public Guid AccountId { get; set; }
        public string TransactionType { get; set; }
        public string TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
